import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ListDoctor from '@/views/ListDoctor.vue'
import Trainings from '@/views/Trainings.vue'
import Patients from '@/views/Patients.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/list-doctor',
    name: 'list-doctor',
    component: ListDoctor
  },
  {
    path: '/staff-trainings',
    name: 'staff-trainings',
    component: Trainings
  },
  {
    path: '/patients',
    name: 'patients',
    component: Patients
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
