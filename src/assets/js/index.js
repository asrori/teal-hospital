/* eslint-disable */

export default {
  render () {
		$(document).ready(function(){
			$('.dropdown-menu a.dropdown-toggle').on('click', function (e){
				var $el = $(this);
				var $parent = $(this).offsetParent(".dropdown-menu");
				if (!$(this).next().hasClass('show')){
					$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
				}
				var $subMenu = $(this).next(".dropdown-menu");
				$subMenu.toggleClass('show');

				$(this).parent("li").toggleClass('show');

				$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e){
					$('.dropdown-menu .show').removeClass("show");
				});

				if (!$parent.parent().hasClass('navbar-nav')){
					$el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
				}
				return false;
			});
		});

		// Textarea characters left
		$(function() {
			$('#characterLeft').text('140 characters left');
			$('#message').keydown(function () {
				var max = 140;
				var len = $(this).val().length;
				if (len >= max) {
					$('#characterLeft').text('You have reached the limit');
					$('#characterLeft').addClass('red');
					$('#btnSubmit').addClass('disabled');
				}
				else {
					var ch = max - len;
					$('#characterLeft').text(ch + ' characters left');
					$('#btnSubmit').removeClass('disabled');
					$('#characterLeft').removeClass('red');
				}
			});
		});
	}
}
